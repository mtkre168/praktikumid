package snekk;

import java.awt.Color;
import java.awt.Graphics;

public class BodyPart {
	private int x, y, width, height;
	
	public BodyPart(int x, int y, int tileSize) {
		this.x = x;
		this.y = y;
		width = tileSize;
		height = tileSize;
	}
	
	//tegin 2 eraldi funktsiooni, et pea v�rviks eraldi rohelisena ja keha tume rohelisena
	public void draw(Graphics g) {
		g.setColor(Color.GREEN.darker().darker().darker());
		g.fillOval(x * width, y * height, width, height);
	}
	public void draw2(Graphics g) {
		g.setColor(Color.GREEN);
		g.fillOval(x * width, y * height, width, height);
		
	}

	public int getX() {
		return x;
	}


	public void setX(int x) {
		this.x = x;
	}


	public int getY() {
		return y;
	}


	public void setY(int y) {
		this.y = y;
	}
	
}
