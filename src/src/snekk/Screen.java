package snekk;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Screen extends JPanel implements Runnable, KeyListener {

	private static final long serialVersionUID = 1L;

	public static final int WIDTH = 300, HEIGHT = 270;
	public JFrame GUI;
	private Thread thread;
	private boolean paused, running = false;
	private int i, x = 10, y = 10, xl, yl, n, primetime;
	private int size = 5;

	private boolean right = false, left = false, up = false, down = true;

	private int ticks = 0, ticksT = 0, time = 0, score, timer, speed = 1000000;
	public Random random;
	public Point food, lunch;

	private BodyPart b;
	private ArrayList<BodyPart> sneik;

	public Screen() {
		setFocusable(true);
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		addKeyListener(this);
		sneik = new ArrayList<BodyPart>();

		start();
		gui();
	}

	//Kiiruse valiku men�� "Settings", saab avada suvalisel ajal "M" t�hega 
	//ja saab kiirust vahetada ka pela ajal jne
	public void gui() {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		JFrame GUI = new JFrame("Settings");
		GUI.setVisible(true);
		GUI.setSize(500, 75);
		GUI.setLocation(dim.width / 2 - GUI.getWidth() / 2, dim.height / 5 - GUI.getHeight() / 2);
		GUI.setResizable(false);
		GUI.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		GUI.addKeyListener(this);

		
		//Teeb JPanel p peale nupud ja jutud
		JPanel p = new JPanel();
		JLabel lab1 = new JLabel("Pick your speed!");
		JButton b1 = new JButton("NOOB");
		JButton b2 = new JButton("NORMAL");
		JButton b3 = new JButton("ADVANCED");
		JButton b4 = new JButton("2FAST4U");

		/*b1 nupule, nametagiga "NOOB" setib speed=2000000 ehk ticks > 2000000,
		 *ehk iga 2000000 ticks tagant liigub
		 *
		 *tick meetod k�ib suht kiirelt sellest tulenevad ka suured arvud
		 *teised nupud samal p�him�ttel
		*/
		b1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				speed = 2000000;
				JOptionPane.showMessageDialog(null, "Your speed is NOOB ");
			}
		});
		b2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				speed = 1000000;
				JOptionPane.showMessageDialog(null, "Your speed is NORMAL");
			}
		});
		b3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				speed = 750000;
				JOptionPane.showMessageDialog(null, "Your speed is ADVANCED");
			}
		});
		b4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				speed = 500000;
				JOptionPane.showMessageDialog(null, "Your speed is 2FAST4U");
			}
		});

		p.add(b1);
		p.add(b2);
		p.add(b3);
		p.add(b4);
		p.add(lab1);
		GUI.add(p);

	}

	//start(); paneb m�ngu alguseks muutujad ja asjad paika, kutsub seda v�lja alustades ja p�rast game overit.
	public void start() {
		sneik.clear();
		running = false;
		paused = true;
		score = 0;
		time = 0;
		timer = 0;
		size = 5;
		n = 1;
		primetime = 0;
		x = 10;
		y = 10;
		random = new Random();
		food = new Point(random.nextInt(WIDTH / 10 - 1), random.nextInt(HEIGHT / 10 - 1));
		thread = new Thread(this, "Game loop");
		thread.start();
	}

	public void tick() {

		// kui run(); kutsub v�lja tick(); siis iga kord teeb ticks++ ja ticksT++
		ticks++;
		ticksT++;
		// m�ngu alustades on sneik.size 0 ning siis j�rjest tekitab kuni size=5ni.
		if (sneik.size() == 0) {
			b = new BodyPart(x, y, 10);
			sneik.add(b);
		}
		// kui snake pea ehk x ja y l�heb kehale pihta siis running ja paused false ehk kutsub gameover();
		for (int i = 0; i < sneik.size(); i++) {
			if (x == sneik.get(i).getX() && y == sneik.get(i).getY()) {
				if (i != sneik.size() - 1) {
					gameover();
				}
			}
		}
		// kui s�idab piiridest v�lja siis kutsub gameover();
		if (x < 0 || x > (WIDTH / 10 - 1) || y < 0 || y > (HEIGHT / 10 - 1)) {
			gameover();
		}
		
		// ticksT tegin eraldi timeri jaoks, ja sai �ra kasutada ka suure s��gi jaoks,
		// mis omakorda annab advantage'i ka suurema kiirusega m�ngides, sest suure foodini j�uab kiiremini.
		// Timer v�i stopper mis iganes seda kutsuda pole 100% t�pne sekunditega.
		if (ticksT > 2500000 && running == true && paused == false) {
			time++;
			if (time != 0 && size == 7 * n && primetime <= 30) {
				primetime = primetime + 1 * 2;
			}
			ticksT = 0;
			timer = time / 5;
		}

		//liikumine ja muu
		if (ticks > speed && running == true && paused == false) {
			if (right)
				x++;
			if (left) 
				x--;
			if (up)
				y--;
			if (down)
				y++;

			ticks = 0;

			//tekitab snake peast sabani.
			b = new BodyPart(x, y, 10);
			sneik.add(b);
			// ei lase suuremat madu teha kui size on
			if (sneik.size() > size) {
				sneik.remove(0);
			}
			//s��gi kokkupuutel saab juurde 10 skoori, 1 size ja tekitab uue foodi random kohta
			if (food != null && time != 0) {
				if (x == food.x && y == food.y) {
					// juhul kui suur s��k on, ja s��d ikkagi tavalise s��gi, 
					//siis n suureneb, et j�rgmine kord ka tekitaks suure foodi
					if (lunch != null) {
						n++;
					}
					size++;
					score += 10;
					food.setLocation(random.nextInt(WIDTH / 10 - 1), random.nextInt(HEIGHT / 10 - 1));
				}
			} else {
				food.setLocation(random.nextInt(WIDTH / 10 - 1), random.nextInt(HEIGHT / 10 - 1));
			}

			// iga 12 size tagant tekib suur s��k mis kestab umbkaudu 3 sekundit
			if (time != 0 && size == 7 * n && primetime <= 30) {

				// tekitab suure s��gi siis ainult juhul kui �lemine if statement kehtib
				lunch = new Point(xl, yl);
				if (lunch != null) {
					// kui aeg otsa jookseb siis nullib primetime �ra, et j�rgmine kord kasutada
					// ning n++, et j�rgmise 12 size p�rast uuesti suure s��gi teeks
					if (primetime == 30) {
						primetime = 0;
						n++;
					}
					// suure s��gi jaoks 4 "koordinaati" v�i punkti,
					// kuhu pihta minnes saab juurde 30 skoori, 5 size
					// kaotab suure s��gi ehk lunchi �ra, lisab n juurde
					// randomizib j�rgmise locationi suurele s��gile.
					if (x == lunch.x && y == lunch.y || x == lunch.x + 1 && y == lunch.y
							|| x == lunch.x && y == lunch.y + 1 || x == lunch.x + 1 && y == lunch.y + 1) {
						size += 5;
						score += 30;
						lunch = null;
						n += 1;
						primetime = 0;
					}

				}
			} else {
				xl = random.nextInt(WIDTH / 10 - 2);
				yl = random.nextInt(HEIGHT / 10 - 2);
				lunch = null;
			}

		}
	}

	// joonistab ja v�rvib backgroundi, gridi, sneiki, s��gi, suure s��gi
	// �lesse kirjutab skoori, size ehk length ja timer ning juhul kui suur s��k,
	// siis n�itab ka kaua suur s��k kestab
	//sneikki v�rvib BodyPart all
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, WIDTH, HEIGHT);
		g.setColor(Color.GRAY.darker().darker().darker().darker());
		for (int i = 0; i < WIDTH / 10; i++) {
			g.drawLine(i * 10, 0, i * 10, HEIGHT);
		}
		for (int i = 0; i < HEIGHT / 10; i++) {
			g.drawLine(0, i * 10, WIDTH, i * 10);
		}
		//pea joonistab eraldi ja keha j�rgi
		for (int i = 0; i < sneik.size(); i++) {
			if (i == sneik.size() - 1) {
				sneik.get(i).draw2(g);
			} else {
				sneik.get(i).draw(g);
			}
		}
		g.setColor(Color.RED);
		if (time != 0) {
			g.fillOval(food.x * 10, food.y * 10, 10, 10);
		}
		g.setColor(Color.YELLOW);
		if (time != 0 && lunch != null && size == 7 * n && primetime <= 30) {
			g.fillOval(lunch.x * 10, lunch.y * 10, 20, 20);
		}

		g.setColor(Color.WHITE);
		String string = "Score: " + score + ", Length: " + size + ", Time: " + timer;
		if (lunch != null && primetime != 0) {
			string = "Score: " + score + ", Length: " + size + ", Time: " + timer + ", LUNCH: " + (3 - primetime / 10);
		}
		g.drawString(string, (int) (getWidth() / 2 - string.length() * 3f), 10);

		string = "Start the game!";

		if (time == 0) {
			g.drawString(string, (int) (getWidth() / 2 - string.length() * 3f), (int) getHeight() / 4);
		}

		string = "Game over!";

		if (running == false && paused == false) {
			g.drawString(string, (int) (getWidth() / 2 - string.length() * 3f), (int) getHeight() / 4);
		}

		string = "Paused!";

		if (running == false && paused == true && time > 0) {
			g.drawString(string, (int) (getWidth() / 2 - string.length() * 3f), (int) getHeight() / 4);
		}
	}

	public void gameover() {
		running = false;
		paused = false;
	}

	public void pause() {
		running = false;
		paused = true;
	}

	public void resume() {
		running = true;
		paused = false;
		thread = new Thread(this, "Game loop");
		thread.start();
	}

	public void run() {
		while (running == true && paused == false) {
			tick();
			repaint();
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {

		i = e.getKeyCode();

		if ((i == KeyEvent.VK_M)) {
			gui();
		}

		if ((i == KeyEvent.VK_A || i == KeyEvent.VK_LEFT) && !right) {
			up = false;
			down = false;
			left = true;
		}

		if ((i == KeyEvent.VK_D || i == KeyEvent.VK_RIGHT) && !left) {
			up = false;
			down = false;
			right = true;
		}

		if ((i == KeyEvent.VK_W || i == KeyEvent.VK_UP) && !down) {
			up = true;
			left = false;
			right = false;
		}

		if ((i == KeyEvent.VK_S || i == KeyEvent.VK_DOWN) && !up) {
			down = true;
			left = false;
			right = false;
		}
		if (i == KeyEvent.VK_ENTER) {
			if (running == false && paused == true && time == 0) {
				start();
				time++;
				running = true;
				paused = false;
			} else if (running == true && paused == false && time > 0) {
				pause();
			} else if (running == false && paused == true && time > 0) {
				resume();
				repaint();
			} else if (running == false && paused == false) {
				repaint();
				start();
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
	}

}
