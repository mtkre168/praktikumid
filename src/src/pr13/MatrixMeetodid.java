package pr13;

public class MatrixMeetodid {
	public static void main(String[] args) {
		int[][] neo = { { 1, 1, 1, 1, 1 }, { 2, 3, 4, 5, 6 }, { 3, 4, 5, 6, 7 }, { 4, 5, 6, 7, 8 },
				{ 5, 6, 7, 8, 9 }, };

		int i, j;
		int sum = 0;

		// Trykime maatriksi va"lja
		for (i = 0; i < neo.length; i = i + 1) {
			for (j = 0; j < neo[i].length; j = j + 1) {
				System.out.print(neo[i][j] + " ");
			}
			System.out.println();
		}

		// Liidame kokku peadiagonaali elemendid
		for (i = 0; i < neo.length; i = i + 1) {
			sum = sum + neo[i][i];
		}
		System.out.println(sum);

		// Transponeerimine
		int[][] morpheus = new int[neo[0].length][neo.length];

		for (i = 0; i < neo.length; i = i + 1) {
			for (j = 0; j < neo[i].length; j = j + 1) {
				morpheus[j][i] = neo[i][j];
			}
		}

		// Trykime maatriksi va"lja
		for (i = 0; i < morpheus.length; i = i + 1) {
			for (j = 0; j < morpheus[i].length; j = j + 1) {
				System.out.print(morpheus[i][j] + " ");
			}
			System.out.println();
		}
		// tryki(neo);
		tryki1(ridadeSummad(neo));
		System.out.println(korvalDiagonaaliSumma(neo));

	}

	public static void tryki1(int[] massiiv) {
		for (int arv : massiiv) {
			System.out.print(arv + " ");
		}
		System.out.print("\n");
	}

	public static void tryki(int[][] maatriks) {
		for (int[] rida : maatriks) {
			tryki1(rida);
		}

		for (int i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks.length; j++) {
				System.out.print(maatriks[i][j] + " ");
			}
			System.out.println("");
		}
	}

	public static int[] ridadeSummad(int[][] maatriks) {
		int[] summad;
		int col = maatriks[0].length;
		summad = new int[col];
		for (int i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks.length; j++) {
				summad[i] += maatriks[i][j];
			}
			// System.out.println("rea summa: " + summad[]);
		}
		return summad;
	}

	public static int korvalDiagonaaliSumma(int[][] maatriks) {
		int summa = 0;
		for (int i = maatriks.length - 1; i > 0; i--) {
			for (int j = maatriks.length - 1; j > 0; j--) {
				summa += maatriks[i][j];
			}
			// System.out.print(maatriks[i][j] + " ");
		}
		return summa;
	}
	/*public static int[] ridadeMaksimumid(int[][] maatriks){
		int max = Integer.MIN_VALUE;
		for(int i = 0; i < maatriks.length; i++) {
		      if(maatriks[i] > max) {
		         max = maatriks[i];
		      }
	}*/
}
