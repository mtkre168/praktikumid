package praktikum2;

import java.util.Scanner;

public class Muudabt2he {
	public static void main(String[] args) {

		Scanner userInput = new Scanner(System.in);

		String nimi;
		System.out.print("Kirjuda nimi (muudab 'a' tähed '_'-ks): ");
		nimi = userInput.next();
		nimi = nimi.replace('a', '_');
		// System.out.print("Nimi nyyd on : " );
		// System.out.println(nimi.replace('a', '_'));
		System.out.println("'a' tähed on kadunuuud: " + nimi);
		userInput.close();
		return;
	}
}
