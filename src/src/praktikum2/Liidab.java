package praktikum2;

import java.util.Scanner;

public class Liidab {
	public static void main(String[] args) {

		Scanner userInput = new Scanner(System.in);

		int x = 1;
		int c = 0;

		while (x != 0) {
			System.out.print("Sisesta täisarv (liidab - 0 juhul lõpetab): ");
			x = userInput.nextInt();

			if (x != 0) {
				c += x;
				System.out.println("Hetke summa: " + c);
			}

		}
		System.out.println("Kogu Summa: " + c);
		userInput.close();
	}
}
