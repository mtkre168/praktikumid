package praktikum2;

import java.util.Scanner;

public class Ruutjuur {
	public static void main(String[] args) {

		Scanner userInput = new Scanner(System.in);

		float x = 0;
		float y;
		int k = 0;
		float z;
		while (x <= 0) {
			System.out.print("Sisesta positiivne reaalarv: ");
			x = userInput.nextInt();
		}
		y = (x + 1) / 2;
		System.out.println("1. samm Y = " + y);
		System.out.println("------------------------");
		while (k == 0) {
			y = (y + x / y) / 2;
			System.out.println("2. samm Y = " + y);
			z = y * y;
			System.out.println("------------------------");
			if (Math.abs(z - x) < 0.0001) {
				System.out.println("3. samm Y = " + y);
				k++;
				return;
			}
			userInput.close();
		}
	}
}

/*
 * Koostage Java-programm, mis küsib kasutajalt positiivse reaalarvu x (kui
 * sisestatakse vale arv, siis küsimust korratakse kuni positiivse tulemuse
 * saamiseni). Leida arvust x ruutjuur järgmise meetodiga:
 * 
 * Arvutada alglähend y = (x+1)/2. Olemasoleva y põhjal arvutada järgmine lähend
 * y = (y + x/y )/2. Kui leitud y ruut erineb arvust x vähem, kui 0.0001 võrra,
 * siis väljastada tulemus y ja lõpetada töö. Vastasel korral minna sammule 2.
 * 
 * Testida programmi etteplaneeritud testidega.
 */
