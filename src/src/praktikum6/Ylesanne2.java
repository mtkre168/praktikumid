package praktikum6;

import java.util.Scanner;

public class Ylesanne2 {
	public static void main(String[] args) {

		int min = 5;
		int max = 29;

		Sisestus(min, max);

		return;

	}

	public static void Sisestus(int min, int max) {
		int i;
		boolean chekk = true;
		while (chekk == true) {
			Scanner in = new Scanner(System.in);
			System.out.println("Sisesta number:");
			i = in.nextInt();
			if (i > min && i < max) {
				System.out.println("Sisestasid õige arvu (" + i + ")");
				in.close();
				chekk = false;
			} else {
				System.out.println("Sisestasid vale arvu, proovi uuesti");

			}

		}

	}
}