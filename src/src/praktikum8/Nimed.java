package praktikum8;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Nimed {
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		ArrayList<String> nimed = new ArrayList<String>();
		
		System.out.print("Sisestage nimed (tühja rea sisestamine lõpetab töö): ");
		while(true) {
			String nimi = in.nextLine();
			if(nimi.length()==0){
				in.close();
				break;
			}
			
			nimed.add(nimi);
		}
		Collections.sort(nimed);
		System.out.println(nimed);
	}
}