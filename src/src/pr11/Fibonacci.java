package pr11;

public class Fibonacci {
	public static void main(String[] args) {
		int number = 12;
		for (int i = 1; i <= number; i++) {
			System.out.println(fibonacciRecusion(i) + " ");
		}
		
	}
	public static int fibonacciRecusion(int number) {
		if (number == 1 || number == 2) {
			return 1;
		}
 
		return fibonacciRecusion(number - 1) + fibonacciRecusion(number - 2);
	}
}
