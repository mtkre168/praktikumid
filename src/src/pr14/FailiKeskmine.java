package pr14;

import java.util.ArrayList;

public class FailiKeskmine {
	public static void main(String[] args) {
		ArrayList<String> failiSisu = FailiLugeja.readFile("numbrid.txt");
		System.out.println(failiSisu);
		arvudeKeskmine(failiSisu);

	}

	public static double arvudeKeskmine(ArrayList<String> failiSisu) {

		double amount = 0;
		for (int i = 0; i < failiSisu.size(); i++)
			try {
				{
					amount = amount + Double.valueOf((String) failiSisu.get(i));
				}
			} catch (NumberFormatException e) {
				System.out.println("Mingi kala on siin: " + e.getMessage());
			}
		System.out.println("Arvude summa: " + amount);
		double x = amount / failiSisu.size();
		System.out.println("Arit. keskmine: " + x);
		return amount;
	}
}
