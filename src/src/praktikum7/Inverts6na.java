
package praktikum7;

import java.util.Scanner;

public class Inverts6na {
	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		System.out.println("Kirjuta mingi sõna: ");
		String s6na = in.next();
		String s6na2;
		s6na2 = reverseIt(s6na);
		System.out.println("Reversed: " + s6na2);
		in.close();
	}

	public static String reverseIt(String source) {
		int i, x = source.length();
		StringBuilder xd = new StringBuilder(x);

		for (i = (x - 1); i >= 0; i--) {
			xd.append(source.charAt(i));
		}

		return xd.toString();
	}
}