package praktikum7;

import java.util.Scanner;

public class Arvamism2ng {

	public static int random() {
		int rnd = (int) ((Math.random() * 101));
		return rnd;
	}

	public static void main(String[] args) {
		int player;
		int pc = random();
		Scanner in = new Scanner(System.in);
		while (true) {
		System.out.println("Sisesta number:");
		player = in.nextInt();
		//System.out.println("ARVUTI NUMBER " + pc);

			if (player == pc) {
				System.out.println("Arvasid ära, gg");
				System.out.println("Arvuti number oli " + pc);
				break;
			} else if (player > pc) {
				System.out.println("Liiga suur arv! Proovi uuesti");
			} else {
				System.out.println("Liiga väike arv! Proovi uuesti");
				continue;
			}
		}
		in.close();
	}

}
