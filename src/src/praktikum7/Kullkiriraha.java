package praktikum7;

import java.util.Scanner;

public class Kullkiriraha {
	public static void main(String[] args) {


		int kasutajaRaha = 100;

		Scanner in = new Scanner(System.in);
		while (kasutajaRaha > 0) {

			System.out.println("Sul on " + kasutajaRaha + " raha");
			System.out.println("Sisesta oma panus (max 25): ");
			int panus = in.nextInt();
			if (panus <= 0 || panus > 25) {
				System.out.println("Sisestasite imeliku panuse. Uuesti!");
				continue;
			}
			kasutajaRaha -= panus;

			int myndivise = (int) (Math.random() * 2);
			
			if (1==myndivise) {
				System.out.println("Tuli kiri, saad topelt rahhi");
				kasutajaRaha += panus * 2;

			} else {
				System.out.println("Tuli kull, feil");
			}
		}
		in.close();
		System.out.println("Raha otsas, tsau");
	}

}