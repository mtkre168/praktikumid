package praktikum7;

import java.util.Scanner;

public class T2ringuraha {
	
	public static void main(String[] args) {


		int kasutajaRaha = 100;

		Scanner in = new Scanner(System.in);
		while (kasutajaRaha > 0) {

			System.out.println("Sul on " + kasutajaRaha + " raha");
			System.out.println("Millise täringu numbri peale panuse paned (1..6): ");
			int number = in.nextInt();
			System.out.println("Sisesta oma panus (max 25): ");
			int panus = in.nextInt();
			if (panus <= 0 || panus > kasutajaRaha) {
				System.out.println("Sisestasite imeliku panuse. Uuesti!");
				continue;
			}
			kasutajaRaha -= panus;

			int t2ring = (int) ((Math.random() * 6) +1);
			
			if (number==t2ring) {
				System.out.println("hea töö, panid pullilt");
				System.out.println("täringul on " + t2ring);
				kasutajaRaha += panus * 6;

			} else {
				System.out.println("mdeaa noo");
				System.out.println("täringul on " + t2ring + ", dayn");
			}
		}
		in.close();
		System.out.println("Raha otsas, tsau");
	}

}