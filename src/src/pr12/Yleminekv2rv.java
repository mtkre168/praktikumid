package pr12;

import java.applet.Applet;
import java.awt.*;

public class Yleminekv2rv extends Applet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void paint(Graphics g) {

		// Graphics2D g2d = (Graphics2D) g;
		int w = getWidth();
		int h = getHeight();
		/*
		 * Color color1 = Color.WHITE; Color color2 = Color.BLACK; GradientPaint
		 * gp = new GradientPaint(0, 0, color1, 0, h, color2); g2d.setPaint(gp);
		 * g2d.fillRect(0, 0, w, h);
		 */

		for (int y = 0; y < h; y++) {
			double concentrate = (double) y / h;
			int a = (int) (255 - (concentrate * 255));
			Color color = new Color(a, a, a);
			g.setColor(color);
			g.drawLine(0, y, w, y);
		}
	}
}
