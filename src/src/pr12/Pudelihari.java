package pr12;

import java.applet.Applet;
import java.awt.*;

public class Pudelihari extends Applet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/*
	 * Ringjoone vo~rrand parameetrilisel kujul x = r * cos(t) y = r * sin(t) t
	 * = -PI..PI
	 */
	public void paint(Graphics g) {

		// setSize(600, 400); //saab settida appleti windowi suurust

		int x0; // Keskpunkt
		int y0;
		int r = 100; // Raadius
		int x, y;
		double t;
		Dimension d = getSize();
		// Kysime kui suur aken on?
		int w = getWidth();
		int h = getHeight();
		x0 = d.width / 2;
		y0 = d.height / 2;
		// Ta"idame tausta
		g.setColor(Color.black);
		g.fillRect(0, 0, w, h);

		// Joonistame
		g.setColor(Color.green);

		for (t = -Math.PI; t < Math.PI; t = t + Math.PI / 16) {
			x = (int) (r * Math.cos(t) + x0);
			y = (int) (r * Math.sin(t) + y0);
			g.drawLine(x0, y0, x, y);
		}
	}
}