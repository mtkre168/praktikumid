package pr12;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

/**
 * Ringjoone valemi jÃ¤rgi ringi joonistamise nÃ¤ide
 * 
 * @author Mikk Mangus
 */
@SuppressWarnings("serial")
public class Spiral extends Applet {

	private Graphics g;

	public void paint(Graphics g) {
		this.g = g;
		background();
		drawSpiral();
	}

	/**
	 * background white
	 */
	public void background() {
		int w = getWidth();
		int h = getHeight();
		g.setColor(Color.white);
		g.fillRect(0, 0, w, h);
	}

	public void drawSpiral() {
		g.setColor(Color.black);
		int keskkohtX = getWidth() / 2;
		int keskkohtY = getHeight() / 2;
		//double raadius = 0;
		Polygon spiraal = new Polygon();
		
		for (double nurk = 0; nurk <= Math.PI * 10; nurk = nurk + .03) {
			double raadius = 10 + nurk * 2;
			//raadius += 0.1;
			int x = (int) (raadius * Math.cos(nurk));
			int y = (int) (raadius * Math.sin(nurk));
			spiraal.addPoint(x, y);
			//g.fillRect(keskkohtX + x, keskkohtY + y, 2, 2);
		}
	}
}