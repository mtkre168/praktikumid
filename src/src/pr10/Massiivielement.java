package pr10;

public class Massiivielement {
	public static void main(String[] args) {
		int[] massiiv = { 1, 3, 6, 7, 8, 3, 5, 7, 21, 3 };
		int suurim = 0;
		for (int i = 0; i < massiiv.length; i++) {
			if (suurim < massiiv[i]) {
				suurim = massiiv[i];
			}
		}
		System.out.println(suurim);
	}
}